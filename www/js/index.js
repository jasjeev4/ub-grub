/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.

 */

var menu;

var curZone = "C3";

var zones = ["C3","MS","GO"];
var zoneMapping = [["C3", "C3"], ["MS", "Main Street"], ["GO", "Governers Dining"]];

var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        setLocalStorage();
        start();
    }
};

app.initialize();

function setLocalStorage() {
    if (localStorage.getItem("liked") === null) {
        var newArr = [];
        localStorage.setItem("liked", JSON.stringify(newArr));
    }
}

function start() {
    //$('#para').text("test");
    //load menu'

    $(document).ready(function() {

        $('.buttons').click(function(){
            var id = $(this).attr('id');
            if(id == "rightBut")
                pageRight();
            else
                pageLeft();
        })

        var ur = "http://jsanand.com/ub-menu.json"
        $.ajax({
                url: ur,
                type:'get',
                dataType:"json",
                success: function(json)
                {
                    menu = json;
                    loadMenu();

                    $(document).on('click', ".fav-heart", function() { 
                        var item = $(this).attr('data-item');
                        var liked = $(this).attr('data-liked');
                        $icon = $(this).children('.fa');
                        if(liked == "true") {
                            removeLiked(item);
                            $icon.removeClass('fa-heart');
                            $icon.addClass('fa-heart-o');
                            $(this).attr('data-liked', 'false');
                        }
                        else {
                            addLiked(item);
                            $icon.removeClass('fa-heart-o');
                            $icon.addClass('fa-heart');
                            $(this).attr('data-liked', 'true');
                        }
                    });
                },
                error: function(err)
                {   
                    alert("There was an error loading the menu. Check your network connection");
                }
            });
    });
}

function loadMenu() {
    var hallName = zoneLongName();
    $('#hall').text(hallName);

    var hallMenu = menu[curZone].Dinner; //only working with Dinner for now
    
    var colorCounter = 1;

    for(var zone in hallMenu) {
        var zoneDiv = '<div class="zone color'+ colorCounter +'">'+ zone +'</div>';
        $('#meal-info').append(zoneDiv);

        colorCounter++;

        //add food items'
        var listId = zone + '-items';
        listId = listId.replace(/\s+/g, '-').toLowerCase();
        var list = '<ul class="list-group" id="' + listId +'"></ul>';
        $('#meal-info').append(list);

        var zoneMenu = hallMenu[zone];
        for(var i=0; i<zoneMenu.length; i++) {
            var item = zoneMenu[i];
            var ul;
            if(isLiked(item))
                ul = '<li class="list-group-item list-item"><span class="fav-heart" data-item="'+ item + '" data-liked="true"><i class="fa fa-heart customRed"></i></span><div>'+ item +'</div></li>';
            else    
                ul = '<li class="list-group-item list-item"><span class="fav-heart" data-item="'+ item + '" data-liked="false"><i class="fa fa-heart-o customRed"></i></span><div>'+ item +'</div></li>';
            $('#' + listId).append(ul);

        }
    }

    

    updateLiked();
}

function addLiked(item) {
    var favs = JSON.parse(localStorage.getItem("liked"));
    favs.push(item);
    localStorage.setItem("liked", JSON.stringify(favs));
    updateLiked();
}

function removeLiked(item) {
    var favs = JSON.parse(localStorage.getItem("liked"));
    var index = favs.indexOf(item);
    if (index > -1) {
        favs.splice(index, 1);
    }
    localStorage.setItem("liked", JSON.stringify(favs));
    updateLiked();
}

function isLiked(item) {
    var favs = JSON.parse(localStorage.getItem("liked"));
    for(var i =0; i<favs.length; i++){
        if(item==favs[i])
            return true;
    }
    return false;
}

function updateLiked() {
    clearLiked();

    //get liked from current as array
    var likedArr = [];
    var hallMenu = menu[curZone].Dinner;
    for(var zone in hallMenu) {
        var zoneMenu = hallMenu[zone];
        for(var i=0; i<zoneMenu.length; i++) {
            var item = zoneMenu[i];
            if(isLiked(item)) {
                likedArr.push(item);
            }
        }
    }

    var numLiked = likedArr.length;

    var no = "None";

    if(numLiked == 1)
        no = "One";

    if(numLiked == 2)
        no = "Two";

    if(numLiked == 3)
        no = "Three";

    if(numLiked == 4)
        no = "Four";

    if(numLiked == 5)
        no = "Five";

    if(numLiked>5)
        no = numLiked;

    var numberLikedStr = no + ' of your favorite items are on the menu here.';
    $('#numberLiked').text(numberLikedStr);

    //add items as labels 

    var prevColor = 1;

    for(var i=0; i<likedArr.length; i++) {
        var colorNo = randColor(prevColor);
        var label = '<span class = "label customLabel color' + colorNo +'">'+ likedArr[i] +'</span>';
        $('#likedInfo').append(label);
    }
}

function randColor(prev) {
    if(prev<0)
        return getRandomInt(1, 6);
    else
    {
        while(true) {
            var  rand = getRandomInt(1, 6);
            if(rand != prev)
                return rand;
        }
    }
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function clear() {
    $('#hall').empty();
    clearLiked();

    //$('.fav-heart').unbind('click');

    $('#meal-info').empty();
}

function clearLiked() {
    $('#numberLiked').empty();
    $('#likedInfo').empty();
}

function zoneLongName() {
    for(var i=0; i<zoneMapping.length; i++) {
        if(zoneMapping[i][0] == curZone)
        {
            return zoneMapping[i][1];
        }
    }
}

function pageRight() {
    var index = zones.indexOf(curZone);
    if(index == 2)
        curZone = zones[0];
    else
        curZone = zones[index+1];
    clear();
    loadMenu();
}

function pageLeft() {
    var index = zones.indexOf(curZone);
    if(index == 0)
        curZone = zones[2];
    else
        curZone = zones[index-1];
    clear();
    loadMenu();
}